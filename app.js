// Include stuff
const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('database.db');
const crypto = require('crypto');
const bodyParser = require('body-parser');
const cors = require('cors');

// Setup app
const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(function(req,res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
})

// Start app on port 3000
app.listen(3000, function() {
	console.log('Example app listening on port 3000!');
});

// The most basic endpoint
app.get('/', function (req, res) {
  res.send('Welcome to the SpacePonies api!');
});

// Login endpoint
app.post('/login', function (req, res) {
	const email = req.body.email; // Fetch the entered email
	const password = req.body.password; // Fetch the entered password

	const stmt = db.prepare("SELECT * FROM Users WHERE email = (?)", email); // Create SQL statment
	stmt.get(function(err, row) { // Execute statement
		if (typeof row === 'undefined') { // If nothing was found
			res.status(404).send({"message": "USER_NOT_FOUND"}); // Print error
		} else {
			const hash = crypto.createHash('sha256').update(password + row.email).digest('base64'); // Hash entered password with email
			if (row.password === hash) { // If passwords match
				res.status(200).json({'email' : row.email, 'token' : row.token, "isCompany" : row.iscompany}); // Return success
			}
			else {
				res.status(404).json({"message": "USER_NOT_FOUND"}); // Return failure
			}
		}
	});
});

// Register endpoint
app.post('/register', function(req, res) {
	const email = req.body.email; // Fetch the entered email
	const password = req.body.password; // Fetch the entered password
	const isCompany = req.body.isCompany; // Fetch if it was a company or not

	if (email.trim() == "") {
		res.status(400).json({"message" : "EMAIL_IS_EMPTY"});
	}
	else if (password.trim() == "") {
		res.status(400).json({"message" : "PASSWORD_IS_EMPTY"});
	}
	else if (emailIsInvalid(email)) {
		res.status(400).json({"message" : "BAD_EMAIL"});
	}

	const stmt = db.prepare("SELECT * FROM Users WHERE email = (?)", email); // Create SQL statement

	stmt.get(function(err, row) { // Execute statement
		if (typeof row === 'undefined') { // If no user was found (we can add it)
			const hash = crypto.createHash("sha256").update(password + email).digest('base64'); // Hash password with email
			const token = crypto.createHash("sha256").update(email).digest('base64'); // Hash email (used as token)

			const addstmt = db.prepare("INSERT INTO Users (email, password, token, iscompany) Values (?, ?, ?, ?)", email, hash, token, isCompany); // Create SQL statement
			
			addstmt.run(function(err) { // Execute statement
				if (err == null) { // User was added
					res.status(200).json({"email" : email, "token" : token, "isCompany" : isCompany}); // Return success
				}
				else { // User could not be added
					res.status(400).json({"message" : "USER_COULD_NOT_BE_ADDED"}); //Return failure
				}
			});
		}
		else { // User exists (Can't add user)
			res.status(400).json({"message" : "EMAIL_ALREADY_REGISTERED"}); // Return failure
		}
	});
});

// Tags endpoint
app.get('/tags', function(req, res) {
	const stmt = db.prepare("SELECT * FROM Tags"); // Create SQL statement
	stmt.all(function(err, rows) { // Execute statement
		if (err == null) { // If there was no error
			res.status(200).json(rows); // Return success
		}
		else { // IF there was an error
			res.status(400).json({"message" : "AN_ERROR_OCCURED"}); // Return failure
		}
	});
});

// Add tag endpoint
app.post('/add_tag', function(req, res) {
	const tagName = req.body.tagName;
	const parentId = req.body.parentId;

	const stmt = db.prepare("INSERT INTO Tags (name, parent_id) VALUES (?, ?)", tagName, parentId);
	stmt.run(function(err) {
		if (err == null) {
			res.status(200).json({"message" : "TAG_ADDED"});
		}
		else {
			res.status(400).json({"message" : "COULD_NOT_ADD_TAG"});
		}
	})
});

// Get all tags for a user, endpoint
app.post('/users_tags', function(req, res) {
	const token = req.body.token; // Fetch the token of the caller
	const tags = req.body.tags; // Fetch the list of tags+experience levels

	const stmt = db.prepare("SELECT id FROM Users WHERE token = (?)", token); // Get id of user with token
	stmt.get(function(err, row) { // Execute statement
		if (typeof row == 'undefined') {
			res.status(404).json({"message" : "USER_NOT_FOUND"}); // Return failure
		}
		else if (err != null) {
			res.status(400).json({"message" : "AN_ERROR_OCCURED"}); // Return failure
		}
		else { // Everything is fine
			let userId = row.id; // Set variable userId to the id fetched from the db

			const deletestmt = db.prepare("DELETE FROM Users_Tags WHERE user_id = (?)", userId); // Delete all tags for user with userId
			stmt.run(function(err) {
				if (err != null) {
					res.status(400).json({"message" : "AN_ERROR_OCCURED"}).end();
				} 
				else {
					const insertstmt = db.prepare("INSERT INTO Users_Tags (user_id, tag_id, experience) VALUES (?, ?, ?)"); // Create SQL statement
					for (let i = 0; i < tags.length; i++) { // Go through all tags 
						console.log(i);
						stmt.run(userId, tags[i].tagId, tags[i].experience, function(err) {
							if (err != null) {
								console.log(err);
								res.status(400).json({"message" : "AN_ERROR_OCCURED"}).end();
							}
						}); // Run SQL with the data at the current place in the list of tags
					}	
					res.status(200).json({"message" : "TAGS_ADDED"}); // Return success
				}
			});
		}
	});
});

// Users endpoint (does nothing at the moment)
app.post('/get_user', function(req, res) {
	const token = req.body.token; // Fetch the token from the request

	const userstmt = db.prepare("SELECT id, email, token, iscompany FROM Users WHERE token = (?)", token); // Get user where token is token
	userstmt.get(function(err, row) { // Execute statement
		if (typeof row === 'undefined') {
			res.status(404).json({"message" : "USER_NOT_FOUND"}); // Return failure
		}
		else if (err != null) {
			res.status(400).json({"message" : "AN_ERROR_OCCURED"}); // Return failure
		}
		else {
			const tagsstmt = db.prepare("SELECT * FROM Tags"); // Get all tags
			tagsstmt.all(function(err, rows) { // Execute statement
				if (err != null) {
					res.status(400).json({"message" : "AN_ERROR_OCCURED"}); // Return failure
				}
				else {
					const usertagsstmt = db.prepare("SELECT * FROM Users_Tags WHERE user_id = (?)", row.id); // Get all tags for a specific user
					usertagsstmt.all(function(err, rows2) { // Execute statement
						if (err != null) {
							res.status(400).json({"message" : "AN_ERROR_OCCURED"}); // Return failure
						}

						res.status(200).json({'user' : row, 'tags' : rows, 'userTags' : rows2}); // Return success
					});
				}
			});
		}
	});
});

// Endpoint to get matching users
app.get('/user_matches', function(req, res) {
	res.status(200).json({"message" : "ALL_IS_GOOD_IN_THE_HOOD"});
});

// Endpoint to get matching companies
app.get('/company_matches', function(req, res) {
	res.status(200).json({"message" : "MORE_IS_GOOD_IN_THE_HOOD"});
});

// Validate the email
const emailIsInvalid = function(email) {
	const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/ // Pattern for a valid email
	return !emailRegex.test(email); // Return false if the email follows the pattern
};